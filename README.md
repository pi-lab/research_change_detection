# 差异检测

## 1. 研究目标

差异检测的主要任务是根据在同一场景地点下但拍摄于不同时间前后的一对图片，判断在这一时间段中该场景内发生了哪些变化。最直观的想法是两张图片对应像素相减，像素不为零的区域即为变化区域。但是实际应用中，多数情况下摄像机是架设在移动设备上的。因此，虽然两张图片在同一位置拍摄，但是可能并不能精准对齐。而且受到拍摄时间、季节以及光照等影响，两张图片在像素上也会存在很大的偏差。不同于传统的基于遥感图像的差异检测算法，使用 RGB 图像的差异检测算法需要依赖前期的匹配校准工作。然而，匹配校准工作比较耗时、计算量大，所以使得目前的差异检测算法不能有效且高效的生成预测结果。

针对上述问题开展研究工作，研究设计端到端的深度神经网络，能够通过光流网络自动进行像素级的对齐，然后通过差异检测是否发生变化的区域是用户关系的差异。主要的研究目标有：
1. 由于场景会方法较大的变化，此外季节也会发生较大的变化，能够通过光流网络，将一种图片wrap到另外一张图片，并提供候选Mask。
2. 在获取差异的候选Mask之后，研究微调差异检测网络，根据实际情况对预测的变化区域进行微调，它可以使结果更加关注结构性的变化，而忽略一些由光照、天气等引起的变化。	
3. 整理相关的数据集，设计实验。在不同的数据集上对比研究方法和其他方法，能够证明方法的有效性。



## 2. 主要思路

研究思路和步骤：
1. 先通读一下主要的参考文献，建立对所研究问题的基本认识，了解基本的方法等。
2. 找一些代码运行一下，建立直觉的认识，并熟悉数据集。
3. 可以从基本的深度学习提取特征开始，然后再深入到物体检测、光流网络（参考SfmLearner方法）、U-Net、在线学习方法。
4. 根据研究目标，设计有针对性的网络。


## 3. 关键点

思路、程序、论文撰写分成如下关键点和工作点：

* [ ]  如何检测两张图像之间的像素间的移动，光流网络的原理与实现，以及何种光流网络在复杂度和通用性上能够取得最好的折中效果。
* [ ] 设计差异的建议网络，差异建议网络能够输出对应不上的像素作为候选的差异，此处的网络可以采用类似SfmLearner的网络设计思路，并借鉴损失函数设计。
* [ ] 在获得Mask候选之后，研究Mask微调模块，用于预测不受噪声或任意伪误差影响的变化区域。主要包括Mask细化网络，CRFs层。细化网络主要融合多通道的输入，并实现用户期望差异类型的监督学习。之后通过CRFs再次细化差异的细节，从而得到更好的结果，需要研究如何将CRFs加入到网络。
* [ ] 网络的整体的实现，如何实现模块测试、验证。
    - [ ] 基础网络
    - [ ] Mask微调模块
    - [ ] Mask细化网络
    - [ ] 网络整合
* [ ] 实验数据的收集、整理
    - [ ] 数据集设计，环境类型，标签类型
    - [ ] 数据收集
    - [ ] 打标签
* [ ] 论文写作
    - [ ] 论文整体思路确定
    - [ ] 制定大纲
    - [ ] Introduction，Related Works
    - [ ] Method
    - [ ] Experiments
    - [ ] Conclusion
    - [ ] 论文修改

## 4. 参考代码

* http://192.168.1.3/liqing/mask-CDnet
* http://192.168.1.3/liqing/ChangeDetection


## 5. 参考文献

更多的参考文档可以参考[references](references)目录。

### 5.0 综述
* https://github.com/wenhwu/awesome-remote-sensing-change-detection


### 5.1 神经网络等基本方法

* Unsupervised Learning of Depth and Ego-Motion from Video
* FlowNet: Learning Optical Flow with Convolutional Networks
* U-Net: Convolutional Networks for Biomedical Image Segmentation

### 5.2 基于神经网络的差异检测方法
* Aerial image change detection using dual regions of interest networks
* Mask-CDNet: A mask based pixel change detection network

### 5.3 Dataset
* PCD-2015
* AICD-2012 


### 5.4 其他参考
* [如何做研究](https://gitee.com/pi-lab/pilab_research_fields/blob/master/tips/HowToResearch.md)
* [英文论文写作](https://gitee.com/pi-lab/pilab_research_fields/blob/master/tips/paper_writting/README.md)
* [一步一步学写作](https://gitee.com/pi-lab/pilab_research_fields/blob/master/tips/learn_writting/README.md)
