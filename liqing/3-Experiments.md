# 实验与分析

为了评价提出的差异检测方法的性能，我们在两个公开数据集上评估了我们的方法：PCD-2015数据集（Change Detection from a Street Image Pair using CNN Features and Superpixel Segmentation， 2015）和AICD-2012数据集（Constrained optical flow for aerial image change detection）。以平均交并比（mIoU, mean Intersection over Union, 缩写为mIoU) 和$F_1$得分作为评价标准，并对该方法的性能进行了评价。

$mIoU$度量真值与预测结果的精度。$F_1$分数为精确性和召回率的调和平均值，$F_1$分数在1时达到最佳值（完美的精确性和召回率），在0时则为最差值。



## 1. PCD-2015


### 1.1 数据集和预处理
为了证明所提方法的稳健性，我们在公共数据集PCD-2015上评估了我们的方法，该数据集是最具挑战性的差异检测数据集之一。全景差异数据集（PCD,  Panoramic Change Detection, 缩写为PCD)由两个子集组成，分别称为“TSUNAMI”和“GSV”。‘TSUNAMI’包含日本100张日本海啸区的全景图像。“GSV”由100张谷歌全景图像对组成。这些图像的大小为224*1024。考虑到光照差异对预测结果影响较大，本实验中，采用直方图均衡化的方法，在一定程度上减小了光照的影响。为了解决训练数据不足的问题，我们在原始数据集的图像用224*224大小的滑动窗以56像素向右进行滑动裁剪，生成若干图像块。此外，我们还采用了一些典型的数据增强技术：随机旋转、随机裁剪和添加高斯噪声等。因此，每个子集生成12000多组图像。同时，图像在送入网络之前被规范化为[0, 1]之间。

### 1.2 实施细节

该网络使用Adam优化器进行端到端的训练，$\beta_1=0.9$，$\beta_2=0.999$。为了避免过度拟合，在训练的过程中加入了‘权重衰减’，且系数设置为0.001。学习率设置为$10^{-5}$。所有实验分为20个阶段进行训练，前10个阶段的学习率下降率为0.7。batch大小为16。

实验在含有4块GTX1080Ti的服务器上进行训练。利用flowNetS作为预训练模型对光流估计网络进行初始化，并在第一轮训练过程中冻结光流估计网络的权重，以获得良好的Mask生成网络初始化结果。

在所有的实验中，$\lambda_{s}$设为0.1。另外，光度项（$L_{p}$）和正则化项（$L_{r}$）在训练Mask生成网络时相互竞争。$L_{p}$和$L_{r}$之间的权重将决定网络是倾向于输出尽可能多的差异，还是仅仅输出主要差异。在本实验中，$\lambda_{r}$设为0.08。

全连通CRF的参数设置如下：$\sigma_{\gamma}$设为1。$\sigma_{\alpha}$和$\sigma_{\beta}$分别为49和13。$w_1$为5，$w_2$为4。

此外，我们使用5倍交叉验证来评估方法的性能。全景图像对被随机分成5个大小相等的子样本。保留一个子样本作为验证模型的测试数据，并且其余4个子样本作为训练数据。该过程随后重复5次我们一次记录测试数据的$F_1$分数和平均交集比（mIoU），并计算平均值作为评估结果。

为了验证算法的有效性，还进行了消融实验。“Mask-CDNet(-CRFs)”是指对无CRFs细化的结果评估。“Mask-CDNet（-pm）”是在Mask微调模块的输入为八通道图像（只包含输入图像和光流）的结果。‘Mask-CDNet（-pm -CRFs）’表示输入为八通道图像以及在没有CRFs微调的情况下的结果。‘Mask-CDNet（-op -pm -CRFs）’和‘Mask-CDNet（-op -pm）’分别指Mask微调模块的输入仅为两幅输入图像时使用CRFs细化和未使用CRFs细化的结果。此外，‘Mask-CDNet(-OP)’ 是指Mask微调模块的输入中无光流信息。值得注意的是，上述控制实验的网络是在同一数据集上重新训练的，性能结果是使用同一个评估协议进行评估。


### 1.3 定性结果
“GSV”的定性结果如下图1所示，“TSUNAMI”的结果如下图2所示。从每个结果的第一行，可以发现Mask近似地与图像对之间的化区域相一致。这表明我们的网络已经学会了在没有真值监督信号的情况下，可以预测像素级的差异变化。从每个结果的第二行，我们可以发现Mask-CDNet（-op -pm）和Mask-CDNet都能够在输入的图像仅仅在大致对齐的情况下预测变化的区域。

![](images/image-07-GSV_results.png)
GSV 数据集的场景变化检测结果

![](images/image-08-TSUNAMI_results.png)
TSUNAMI 数据集的场景变化检测结果


此方法可以解决这个问题归功于两个方面。（1）我们使用具有很强捕获深层次相关信息能力的深度神经网络学习层次特征。（2）本方法提出的数据扩充方法使得网络学习多时序图像的匹配关系，而不是学习对少数训练数据的过拟合。同时，实验结果表明，具有Mask先验信息的Mask-CDNet比Mask-CDNet（-op -pm）具有更好的性能。我们认为这是由于光流提供了图像配准的信息来克服粗略对齐的问题，并且建议Mask提供了光流预测不匹配的信息。因此，我们可以得出一个重要结论，即带Mask信息的光流在我们的框架中起着重要的作用。

然而，我们可以从下图中发现仍然存在两个问题：(1)预测结果仍然存在误报;（2）它也会漏检一些小的差异，因为从几个像素获得的信息较少。但从定量结果来看，该方法优于现有方法。


### 1.4 结果比较

#### 1.4.1 定量结果

下面两个表列出了不同方法的定量精度结果。


变化检测的预测精度。本表展示了 TSUNAMI 和 GSV 数据集预测结果的 F1 值。
![](images/image-09-PCD_F1.png)

变化检测的预测精度。本表展示了 TSUNAMI 和 GSV 数据集预测结果的 mIoU。
![](images/image-10-PCD_mIoU.png)

实验结果显示了我们的Mask-CDNet与多个方法的比较结果，包括基于CNN网格特征的方法(Change Detection from a Street Image Pair using CNN Features and Superpixel Segmentation, 2015)、基于deconvnet的方法(Street-View Change Detection with Deconvolutional Networks, 2016)、弱监督网络(Learning deep structured network for weakly supervised change detection, 2017)和DOF-CDNet(Dense optical flow based change detection network robust to difference of camera viewpoints, 2017)。

所有这些方法都是在同一个数据集上训练的，并在相同的评估协议上进行测试。我们的方法和现有的方法比较结果表明，我们的网络性能明显优于其他方法。我们认为获得更好的结果归根于以下方面。
1. 目前，所有基于深度学习的变化检测方法都会提供一对图片的匹配信息 ，以便于网络能够获得检测变化区域的能力，而不是仅仅过拟合训练数据集。与这些方法相比，本文提出的方法利用网络直接获得匹配信息。同时，我们的方法在差异检测任务中引入了建议Mask。不仅有效地克服了匹配误差的问题，而且提供了哪里可能存在不同的先验信息。实验证明了其有效性。
2. 专门设计的“Mask细化网络”适用于变化检测的任务。鉴于目前所有的公共数据集都不是很大，为了提高模型的泛化能力，我们使用了相对较浅的网络而不是非常深的网络。此外，应用“dropout层”和“权重衰减”策略进一步提高了模型的泛化能力。同时，为了在目标边界上获得良好的效果，我们将“反卷积”应用于特征映射，并将其与来自网络“编码部分”的相应特征映射相连接，以便“解码部分”能够获得较低层特征映射提供的精细局部信息。
3. 我们的框架是一个两阶段的方法，但是它是端到端训练的。其优点是整个网络结构可以根据训练数据对所有参数进行微调，以适应特定的任务。这两个阶段可以互相共享信息，后一阶段对前一阶段的估计误差具有鲁棒性。

#### 1.4.2 效率分析

在推理阶段，我们提出的方法不需要重建图像（Warped image），其重新采样过程是很耗时的。因此，在没有CRFs的情况下，我们的方法是一种端到端的网络方法，这样可以更有效的生成Mask。

与文献(Street-View Change Detection with Deconvolutional Networks, 2016) 相比，Mask-CDNet不需要计算相对姿态和重建三维环境。它可以避免使用计算成本昂贵的城市规模的SfM。与文献(Dense optical flow based change detection network robust to difference of camera viewpoints, 2017)不同，它不需要RANSAC操作，这个同样也是计算量大的过程。

此外，在文献(Dense optical flow based change detection network robust to difference of camera viewpoints, 2017)中，使用的传统光流估计方法也是一个耗时的操作。例如，deepflow (DeepFlow: Large Displacement Optical Flow with Deep Matching, 2014)估计一个1024*436图像需要17秒。我们的端到端模型对于一对图像（1024*224）的平均预测时间在没有CRF时只有0.02秒，有CRF进行结果细化后大致仅需0.7秒。

