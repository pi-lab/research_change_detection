CUDA_VISIBLE_DEVICE=1 python main.py \
    --training --model MaskCDnet \
    --loss MaskCDNet_loss --optimizer=Adam --optimizer_lr=1e-5 \
    --save_flow --training_dataset AICD --training_dataset_root ./AICD \
    --resume ./model/FlowNet2-S_checkpoint.pth.tar \
    --training_batch_size 2 --total_epochs 300
