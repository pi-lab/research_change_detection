import numpy as np

def get_iou(pred, gt):
    if pred.shape != gt.shape:
        print 'pred shape', pred.shape, 'gt shape', gt.shape
    assert (pred.shape == gt.shape)
    gt = gt.astype(np.float32)
    pred = pred.astype(np.float32)

    count = np.zeros((2,))
    for j in range(2):
        x = np.where(pred == j)
        p_idx_j = set(zip(x[0].tolist(), x[1].tolist()))

        x = np.where(gt == j)
        GT_idx_j = set(zip(x[0].tolist(), x[1].tolist()))

        n_jj = set.intersection(p_idx_j, GT_idx_j)
        u_jj = set.union(p_idx_j, GT_idx_j)

        if len(GT_idx_j) != 0:
            count[j] = float(len(n_jj)) / float(len(u_jj))

    result_class = count
    mIOU = np.sum(result_class[:]) / float(len(np.unique(gt)))

    return mIOU


def precision(pred, gt):
    p = (pred == 0)
    g = (gt == 0)

    print np.sum(p[p == True]), np.sum(g[g == True])

    tp = np.logical_and(p, g)
    tp = np.sum(tp[tp == True])

    p = (pred == 0)
    g = (gt == 1)
    fp = np.logical_and(p, g)
    fp = np.sum(fp[fp == True])

    print tp, fp

    return 1.0 * tp / (tp + fp)


def recall(pred, gt):
    p = (pred == 0)
    g = (gt == 0)

    tp = np.logical_and(p, g)
    tp = np.sum(tp[tp == True])

    p = (pred == 1)
    g = (gt == 0)
    fn = np.logical_and(p, g)
    fn = np.sum(fn[fn == True])

    return 1.0 * tp / (tp + fn)


def F1(pred, gt):
    prec = precision(pred, gt)
    rec = recall(pred, gt)
    f1 = 2 * ((prec * rec) / (prec + rec))
    return f1