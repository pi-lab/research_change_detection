import os
import numpy as np
from tqdm import tqdm
from torch.autograd import Variable


def train(args, epoch, data_loader, model, optimizer, offset=0):
    model.train()

    if args.save_flow or args.render_validation:
        flow_folder = "{}/{}.epoch-{}-flow-field".format(args.training_dir, args.name.replace('/', '.'), epoch)
        if not os.path.exists(flow_folder):
            os.makedirs(flow_folder)

    args.inference_n_batches = np.inf if args.inference_n_batches < 0 else args.inference_n_batches

    progress = tqdm(data_loader, ncols=100, total=np.minimum(len(data_loader), args.inference_n_batches),
                    desc='Inferencing ',
                    leave=True, position=offset)

    for batch_idx, (data, target) in enumerate(progress):
        data, target = [Variable(d, volatile=False) for d in data], [Variable(t, volatile=False) for t in target]
        if args.cuda:
            data, target = [d.cuda(async=True) for d in data], [t.cuda(async=True) for t in target]

        optimizer.zero_grad()
        losses = model(data[0], target, training=True)
        losses.backward()
        optimizer.step()
        progress.update(1)

        if batch_idx == (args.inference_n_batches - 1):
            break

    progress.close()

    return
