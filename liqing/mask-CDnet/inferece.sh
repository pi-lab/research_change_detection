CUDA_VISIBLE_DEVICE=0 python main.py \
    --inference --model FlowNet2S --save_flow \
    --inference_dataset AICD --inference_dataset_root ./AICD \
    --resume ./result/FlowNet2S_checkpoint.pth.tar \
    --inference_batch_size 2

