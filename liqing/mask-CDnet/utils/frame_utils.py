import numpy as np
from os.path import *
from scipy.misc import imread
import flow_utils 

def read_gen(file_name, is_gt=False):

    if is_gt:
        return imread(file_name, 'L')[:,:,np.newaxis]

    ext = splitext(file_name)[-1]
    if ext == '.png' or ext == '.jpeg' or ext == '.ppm' or ext == '.jpg':
        im = imread(file_name)

        if im.shape[2] > 3:
            return im[:,:,:3]

        else:
            return im
    elif ext == '.bmp':
        im = imread(file_name, 'L')[:,:,np.newaxis]
        return im
    elif ext == '.bin' or ext == '.raw':
        return np.load(file_name)
    elif ext == '.flo':
        return flow_utils.readFlow(file_name).astype(np.float32)
    return []
