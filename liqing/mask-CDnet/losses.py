import torch
import torch.nn as nn

import torch.nn.functional as functional
from torch.autograd import Variable

from networks.resample2d_package.modules.resample2d import Resample2d

def EPE(input_flow, target_flow):
    return torch.norm(target_flow-input_flow,p=2,dim=1).mean()

class L1(nn.Module):
    def __init__(self):
        super(L1, self).__init__()
    def forward(self, output, target):
        lossvalue = torch.abs(output - target).mean()
        return lossvalue

class L2(nn.Module):
    def __init__(self):
        super(L2, self).__init__()
    def forward(self, output, target):
        lossvalue = torch.norm(output-target,p=2,dim=1).mean()
        return lossvalue

class L1Loss(nn.Module):
    def __init__(self, args):
        super(L1Loss, self).__init__()
        self.args = args
        self.loss = L1()
        self.loss_labels = ['L1', 'EPE']

    def forward(self, output, target):
        lossvalue = self.loss(output, target)
        epevalue = EPE(output, target)
        return [lossvalue, epevalue]

class L2Loss(nn.Module):
    def __init__(self, args):
        super(L2Loss, self).__init__()
        self.args = args
        self.loss = L2()
        self.loss_labels = ['L2', 'EPE']

    def forward(self, output, target):
        lossvalue = self.loss(output, target)
        epevalue = EPE(output, target)
        return [lossvalue, epevalue]

class MaskCDNet_loss(nn.Module):
    def __init__(self, args, startScale = 4, numScales = 5, l_weight= 0.32, norm= 'L1'):
        super(MaskCDNet_loss,self).__init__()

        self.startScale = startScale
        self.numScales = numScales
        self.loss_weights = torch.FloatTensor([(l_weight / 2 ** scale) for scale in range(self.numScales)])
        self.args = args
        self.l_type = norm
        self.div_flow = 0.05
        assert(len(self.loss_weights) == self.numScales)

        if self.l_type == 'L1':
            self.loss = L1()
        else:
            self.loss = L2()

        self.multiScales = [nn.AvgPool2d(self.startScale * (2**scale), self.startScale * (2**scale)) for scale in range(self.numScales)]
        self.loss_labels = ['MultiScale-'+self.l_type, 'EPE'],

        self.resample1 = Resample2d()

    def forward(self, output, mask, target, pred_diff, gt):  #output:flow     mask      target:input
        x = torch.cat((target[:,:,0,:,:], target[:,:,1,:,:]), dim = 1)

        s_loss = self.smooth_loss(output)
        e_loss = self.explainability_loss(mask)
        r_loss = self.reconstruction_loss(output, mask, x)
        last_loss = self.regession_loss(pred_diff, gt)

        #print e_loss, r_loss, last_loss

        loss = 0.1 * s_loss + r_loss + 20 * e_loss + last_loss
        print loss
        return loss

    def smooth_loss(self, output):
        def gradient(pred):
            D_dy = pred[:, :, 1:] - pred[:, :, :-1]
            D_dx = pred[:, :, :, 1:] - pred[:, :, :, :-1]
            return D_dx, D_dy

        loss = 0
        weight = 1.

        for scaled_disp in output:
            dx, dy = gradient(scaled_disp)
            dx2, dxdy = gradient(dx)
            dydx, dy2 = gradient(dy)
            loss += (dx2.abs().mean() + dxdy.abs().mean() + dydx.abs().mean() + dy2.abs().mean()) * weight
            weight /= 2.83
        return loss

    def explainability_loss(self, mask):
        loss = 0

        for mask_scaled in mask:
            ones_var = Variable(torch.ones(1)).expand_as(mask_scaled).type_as(mask_scaled)
            #print mask_scaled, ones_var
            loss += functional.binary_cross_entropy(mask_scaled, ones_var)
        return loss


    def reconstruction_loss(self, output, masks, target):

        target_multiscale = []
        for i in range(len(output)):
            target_multiscale.append(self.multiScales[i](target))

        def one_scale(flow, mask, tar):
            # print tar.data.cpu().numpy().shape
            # print flow.data.cpu().numpy().shape
            warp = self.resample1(tar[:, 3:, :, :], flow)
            diff = tar[:, :3, :, :] - warp

            # print diff, mask[:,1:,:,:]

            diff = diff * mask[:, :,:,:].expand_as(diff)


            return diff.abs().mean()

        loss = 0
        for flow, mask, tar in zip(output, masks, target_multiscale):
            loss += one_scale(flow, mask, tar)

        return loss

    def regession_loss(self, pred, target):

        if self.args.training_dataset == "AICD":
            positive = 1200 * (target[0] == 255.0).type(torch.FloatTensor).cuda() * (pred - target[0]/255.)
            negitive = (target[0] == 0.0).cuda().type(torch.FloatTensor).cuda() * (pred - target[0]/255.)
            loss = -1 * positive.sum() + negitive.sum()
            return loss * 0.00001

        elif self.args.training_dataset == "Patch":
            loss_fn = torch.nn.L1Loss(reduce=True, size_average=True)
            loss = loss_fn(pred, target)
            return loss

class CDNetOnly_loss(nn.Module):
    def __init__(self, args, startScale = 4, numScales = 5, l_weight= 0.32, norm= 'L1'):
        super(CDNetOnly_loss,self).__init__()

        self.startScale = startScale
        self.numScales = numScales
        self.loss_weights = torch.FloatTensor([(l_weight / 2 ** scale) for scale in range(self.numScales)])
        self.args = args
        self.l_type = norm

        assert(len(self.loss_weights) == self.numScales)

        if self.l_type == 'L1':
            self.loss = L1()
        else:
            self.loss = L2()

        self.multiScales = [nn.AvgPool2d(self.startScale * (2**scale), self.startScale * (2**scale)) for scale in range(self.numScales)]
        self.loss_labels = ['MultiScale-'+self.l_type, 'EPE'],

        self.resample1 = Resample2d()

    def forward(self, target, pred):
        if self.args.training_dataset == "AICD":
            positive = 1200 * (target[0] == 255.0).type(torch.FloatTensor).cuda() * (pred - target[0]/255.)
            negitive = (target[0] == 0.0).cuda().type(torch.FloatTensor).cuda() * (pred - target[0]/255.)
            loss = -1 * positive.sum() + negitive.sum()
            return loss * 0.00001

        elif self.args.training_dataset == "Patch":
            loss_fn = torch.nn.L1Loss(reduce=True, size_average=True)
            loss = loss_fn(pred, target)
            return loss