from networks.PixelResidual import PixelResidual
from networks.submodules import *
from networks.cdnet import CDnet


class MaskCDnet(nn.Module):
    def __init__(self, args):
        super(MaskCDnet, self).__init__()
        self.pixelresidual = PixelResidual(args, input_channels = 6)
        self.cdnet = CDnet()
        self.upsample1 = nn.Upsample(scale_factor=4, mode='bilinear')

    def forward(self, inputs):

        if self.training:
            x, output, mask = self.pixelresidual(inputs)
            flow = self.upsample1(output[0] * 20)
            mask_up = self.upsample1(mask[0])
            pred_diff = self.cdnet(x, flow, mask_up)

            return x, output, mask, pred_diff

        else:
            x, output, mask, resample = self.pixelresidual(inputs)
            pred_diff = self.cdnet(x, output, mask)
            return output, mask, resample, pred_diff




class CDnetOnly(CDnet):
    def __init__(self, args):
        super(CDnet, self).__init__()

