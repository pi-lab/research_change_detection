from submodules import *

class CDnet(nn.Module):
    def __init__(self):
        super(CDnet, self).__init__()

        self.conv1 = encoder_conv(False,   6,   64, kernel_size=3, stride=1)
        self.conv1_withflow = encoder_conv(False,   9,   64, kernel_size=3, stride=1)

        self.conv2 = encoder_conv(True, 64, 128, kernel_size=4, stride=2)
        self.conv3 = encoder_conv(True, 128, 256, kernel_size=4, stride=2)
        self.conv4 = encoder_conv(True, 256, 512, kernel_size=4, stride=2)
        self.conv5 = encoder_conv(True, 512, 512, kernel_size=4, stride=2)
        self.conv6 = encoder_conv(True, 512, 512, kernel_size=4, stride=2)
        self.conv7 = encoder_conv(True, 512, 512, kernel_size=4, stride=2)
        self.conv8 = encoder_conv(True, 512, 512, kernel_size=4, stride=2)

        self.deconv1 = decoder_deconv(self.training, 512, 512)
        self.deconv2 = decoder_deconv(self.training, 512, 512)
        self.deconv3 = decoder_deconv(self.training, 512, 512)
        self.deconv4 = decoder_deconv(self.training, 512, 512)
        self.deconv5 = decoder_deconv(False, 512, 256)
        self.deconv6 = decoder_deconv(False, 256, 128)
        self.deconv7 = decoder_deconv(False, 128, 64)
        self.deconv8 = nn.ConvTranspose2d(64, 1, kernel_size=3, stride=1, bias=True)


    def forward(self, pic, flow, mask, withflow=True):

        if withflow:
            flow_mean = flow.contiguous().view(flow.size()[:2] + (-1,)).mean(dim=-1).view(flow.size()[:2] + (1, 1))
            flow = (flow - flow_mean) / 255

            x = torch.cat((pic, flow, mask[:,:1,:,:]), dim=1)
            out_conv1 = self.conv1_withflow(x)

        else:
            out_conv1 = self.conv1(pic)

        out_conv2 = self.conv2(out_conv1)
        out_conv3 = self.conv3(out_conv2)
        out_conv4 = self.conv4(out_conv3)
        out_conv5 = self.conv5(out_conv4)
        # out_conv6 = self.conv6(out_conv5)
        # out_conv7 = self.conv7(out_conv6)
        # out_conv8 = self.conv8(out_conv7)
        #
        #
        # out_deconv1 = self.deconv1(out_conv8)
        # out_deconv2 = self.deconv2(out_deconv1)
        # out_deconv3 = self.deconv3(out_deconv2)
        out_deconv4 = self.deconv4(out_conv5)
        out_deconv5 = self.deconv5(out_deconv4)
        out_deconv6 = self.deconv6(out_deconv5)
        out_deconv7 = self.deconv7(out_deconv6)
        out_deconv8 = self.deconv8(out_deconv7)

        return nn.functional.sigmoid(out_deconv8)