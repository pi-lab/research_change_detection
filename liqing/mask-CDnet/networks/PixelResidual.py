import torch
import torch.nn as nn
from torch.nn import init

import math
import numpy as np

from networks.resample2d_package.modules.resample2d import Resample2d

from submodules import *


class PixelResidual(nn.Module):
    def __init__(self, args, input_channels=12, batchNorm=False, div_flow=20):
        super(PixelResidual, self).__init__()

        self.batchNorm = batchNorm
        self.conv1 = conv(self.batchNorm, input_channels, 64, kernel_size=7, stride=2)
        self.conv2 = conv(self.batchNorm, 64, 128, kernel_size=5, stride=2)
        self.conv3 = conv(self.batchNorm, 128, 256, kernel_size=5, stride=2)
        self.conv3_1 = conv(self.batchNorm, 256, 256)
        self.conv4 = conv(self.batchNorm, 256, 512, stride=2)
        self.conv4_1 = conv(self.batchNorm, 512, 512)
        self.conv5 = conv(self.batchNorm, 512, 512, stride=2)
        self.conv5_1 = conv(self.batchNorm, 512, 512)
        self.conv6 = conv(self.batchNorm, 512, 1024, stride=2)
        self.conv6_1 = conv(self.batchNorm, 1024, 1024)

        self.deconv5 = deconv(1024, 512)
        self.deconv4 = deconv(1026, 256)
        self.deconv3 = deconv(770, 128)
        self.deconv2 = deconv(386, 64)

        self.predict_flow6 = predict_flow(1024)
        self.predict_flow5 = predict_flow(1026)
        self.predict_flow4 = predict_flow(770)
        self.predict_flow3 = predict_flow(386)
        self.predict_flow2 = predict_flow(194)

        self.upsampled_flow6_to_5 = nn.ConvTranspose2d(2, 2, 4, 2, 1, bias=False)
        self.upsampled_flow5_to_4 = nn.ConvTranspose2d(2, 2, 4, 2, 1, bias=False)
        self.upsampled_flow4_to_3 = nn.ConvTranspose2d(2, 2, 4, 2, 1, bias=False)
        self.upsampled_flow3_to_2 = nn.ConvTranspose2d(2, 2, 4, 2, 1, bias=False)

        self.upcnv5 = deconv(1024, 512)
        self.upcnv4 = deconv(512, 256)
        self.upcnv3 = deconv(256, 128)
        self.upcnv2 = deconv(128, 64)

        self.mask6 = predict_mask(1024)
        self.mask5 = predict_mask(512)
        self.mask4 = predict_mask(256)
        self.mask3 = predict_mask(128)
        self.mask2 = predict_mask(64)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                if m.bias is not None:
                    init.uniform(m.bias)
                init.xavier_uniform(m.weight)

            if isinstance(m, nn.ConvTranspose2d):
                if m.bias is not None:
                    init.uniform(m.bias)
                init.xavier_uniform(m.weight)
                # init_deconv_bilinear(m.weight)
        self.upsample1 = nn.Upsample(scale_factor=4, mode='bilinear')
        self.resample1 = Resample2d()

        self.rgb_max = args.rgb_max
        self.div_flow = div_flow

    def forward(self, inputs):
        rgb_mean = inputs.contiguous().view(inputs.size()[:2] + (-1,)).mean(dim=-1).view(inputs.size()[:2] + (1, 1, 1,))
        x = (inputs - rgb_mean) / self.rgb_max
        x = torch.cat((x[:, :, 0, :, :], x[:, :, 1, :, :]), dim=1)

        out_conv1 = self.conv1(x)

        out_conv2 = self.conv2(out_conv1)
        out_conv3 = self.conv3_1(self.conv3(out_conv2))
        out_conv4 = self.conv4_1(self.conv4(out_conv3))
        out_conv5 = self.conv5_1(self.conv5(out_conv4))
        out_conv6 = self.conv6_1(self.conv6(out_conv5))

        flow6 = self.predict_flow6(out_conv6)
        flow6_up = self.upsampled_flow6_to_5(flow6)
        out_deconv5 = self.deconv5(out_conv6)

        concat5 = torch.cat((out_conv5, out_deconv5, flow6_up), 1)
        flow5 = self.predict_flow5(concat5)
        flow5_up = self.upsampled_flow5_to_4(flow5)
        out_deconv4 = self.deconv4(concat5)

        concat4 = torch.cat((out_conv4, out_deconv4, flow5_up), 1)
        flow4 = self.predict_flow4(concat4)
        flow4_up = self.upsampled_flow4_to_3(flow4)
        out_deconv3 = self.deconv3(concat4)

        concat3 = torch.cat((out_conv3, out_deconv3, flow4_up), 1)
        flow3 = self.predict_flow3(concat3)
        flow3_up = self.upsampled_flow3_to_2(flow3)
        out_deconv2 = self.deconv2(concat3)

        concat2 = torch.cat((out_conv2, out_deconv2, flow3_up), 1)
        flow2 = self.predict_flow2(concat2)

        # mask:
        mask6 = self.mask6(out_conv6)
        mask_deconv5 = self.upcnv5(out_conv6)

        mask5 = self.mask5(mask_deconv5)
        mask_deconv4 = self.upcnv4(mask_deconv5)

        mask4 = self.mask4(mask_deconv4)
        mask_deconv3 = self.upcnv3(mask_deconv4)

        mask3 = self.mask3(mask_deconv3)
        mask_deconv2 = self.upcnv2(mask_deconv3)

        mask2 = self.mask2(mask_deconv2)

        exp_mask6 = nn.functional.sigmoid(mask6)
        exp_mask5 = nn.functional.sigmoid(mask5)
        exp_mask4 = nn.functional.sigmoid(mask4)
        exp_mask3 = nn.functional.sigmoid(mask3)
        exp_mask2 = nn.functional.sigmoid(mask2)

        if self.training:
            output = [flow2, flow3, flow4, flow5, flow6]
            mask = [exp_mask2, exp_mask3, exp_mask4, exp_mask5, exp_mask6]

            return x, output, mask

        else:
            flow2 = self.upsample1(flow2 * self.div_flow)
            mask2 = self.upsample1(exp_mask2)
            resample = self.resample1(inputs[:, :, 1, :, :], flow2)
            return x, flow2, mask2, resample

