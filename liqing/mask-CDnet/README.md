# mask-CDnet
Change detection by pixel-level and semantic filtering using deep learning. 

## Requirements
The program running on anaconda3 on Linux, which can be installed trrough https://www.anaconda.com/download/#linux

After installed the Anaconda3, please install following packages:
```
# pytorch
pip install pytorch

# OpenCV for python
pip install pyopencv

# python dense CRF
pip install pydensecrf

# Fast, Extensible Progress Meter
pip install tqdm
```


## Download pre-trained model
The pre-trained models used by this program can be downloaded at:


You can also execute the shell `download_pretrained_model.sh` to obtain it.


## Inference
```
CUDA_VISIBLE_DEVICE=0 python main.py \
    --inference --model FlowNet2S --save_flow \
    --inference_dataset AICD --inference_dataset_root ./AICD \
    --resume ./result/FlowNet2S_checkpoint.pth.tar \
    --inference_batch_size 2
```


## Training
```
CUDA_VISIBLE_DEVICE=1 python main.py \
    --training --model MaskCDnet \
    --loss MaskCDNet_loss --optimizer=Adam --optimizer_lr=1e-5 \
    --save_flow \
    --training_dataset AICD --training_dataset_root ./AICD --resume ./model/FlowNet2-S_checkpoint.pth.tar \
    --training_batch_size 2 --total_epochs 300
```
