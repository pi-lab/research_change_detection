from evalute import get_iou, F1
import os
import numpy as np
import time
from torch.autograd import Variable
from tqdm import tqdm
from os.path import *
import cv2
from dense_crf import dense_crf
from utils import flow_utils

def inference(args, epoch, data_loader, model, offset=0):
    model.eval()

    if args.save_flow or args.render_validation:
        flow_folder = "{}/{}.epoch-{}-flow-field".format(args.inference_dir, args.name.replace('/', '.'), epoch)
        if not os.path.exists(flow_folder):
            os.makedirs(flow_folder)

    if not os.path.exists(join(flow_folder, 'pred_pic')):
        os.mkdir(join(flow_folder, 'pred_pic'))
    if not os.path.exists(join(flow_folder, 'with_crf')):
        os.mkdir(join(flow_folder, 'with_crf'))
    if not os.path.exists(join(flow_folder, 'target')):
        os.mkdir(join(flow_folder, 'target'))
    if not os.path.exists(join(flow_folder, 't0')):
        os.mkdir(join(flow_folder, 't0'))
    if not os.path.exists(join(flow_folder, 't1')):
        os.mkdir(join(flow_folder, 't1'))
    if not os.path.exists(join(flow_folder, 'optical')):
        os.mkdir(join(flow_folder, 'optical'))
    if not os.path.exists(join(flow_folder, 'mask')):
        os.mkdir(join(flow_folder, 'mask'))
    if not os.path.exists(join(flow_folder, 'resample')):
        os.mkdir(join(flow_folder, 'resample'))

    args.inference_n_batches = np.inf if args.inference_n_batches < 0 else args.inference_n_batches

    progress = tqdm(data_loader, ncols=100, total=np.minimum(len(data_loader), args.inference_n_batches),
                    desc='Inferencing ',
                    leave=True, position=offset)

    f1 = []
    iou = []

    f1_crf = []
    iou_crf = []

    for batch_idx, (data, target) in enumerate(progress):
        if args.cuda:
            data, target = [d.cuda(async=True) for d in data], [t.cuda(async=True) for t in target]
        data, target = [Variable(d, volatile=True) for d in data], [Variable(t, volatile=True) for t in target]

        # when ground-truth flows are not available for inference_dataset,
        # the targets are set to all zeros. thus, losses are actually L1 or L2 norms of compute optical flows,
        # depending on the type of loss norm passed in
        time_start = time.time()
        output, mask, resample, pred_diff = model(data[0], target, inference=True)
        time_end = time.time()
        print "without:", time_end - time_start

        inputs = data[0]

        pred1 = pred_diff.data.cpu().numpy().transpose(0, 2, 3, 1)
        pred0 = 1 - pred1
        pred_c = np.concatenate((pred0, pred1), axis=3)

        target = target[0] / 255.
        target1 = target.data.cpu().numpy().transpose(0, 2, 3, 1)
        target0 = 1 - target1
        target_c = np.concatenate((target0, target1), axis=3)

        pred_n = np.argmax(pred_c, axis=3)
        gt_n = np.argmax(target_c, axis=3)

        f1_tem = F1(pred_n, gt_n)
        f1.append(f1_tem)
        pred_crf_ = []

        size = target1.shape[0]

        if args.save_flow:
            for i in range(size):
                iou_si = get_iou(pred_n[i], gt_n[i])
                iou.append(iou_si)

                _pflow = output[i].data.cpu().numpy().transpose(1, 2, 0)
                flow_utils.writeFlow(
                    join(flow_folder, 'optical/%06d.flo' % (batch_idx * args.inference_batch_size + i)), _pflow)

                _pflow = mask[i].data.cpu().numpy().transpose(1, 2, 0)[:, :, :1]
                cv2.imwrite(join(flow_folder, 'mask/%06d.png' % (batch_idx * args.inference_batch_size + i)),
                            _pflow * 255)

                _pflow = resample[i].data.cpu().numpy().transpose(1, 2, 0)
                from cvbase.image import rgb2bgr
                cv2.imwrite(join(flow_folder, 'resample/%06d.png' % (batch_idx * args.inference_batch_size + i)),
                            rgb2bgr(_pflow))

                _pflow = pred1[i]
                cv2.imwrite(join(flow_folder, 'pred_pic/%06d.png' % (batch_idx * args.inference_batch_size + i)),
                            _pflow * 255)

                time_start = time.time()
                p = inputs[:, :, 0, :, :][i].data.cpu().numpy().transpose(1, 2, 0)
                p = np.ascontiguousarray(p, dtype=np.uint8)
                _pflow_ = dense_crf(pred_c[i], p)
                time_end = time.time()
                print "withcrf:", time_end - time_start
                cv2.imwrite(join(flow_folder, 'with_crf/%06d.png' % (batch_idx * args.inference_batch_size + i)),
                            _pflow_[:, :, 1] * 255)

                pred_crf = np.argmax(_pflow_, axis=2)
                pred_crf_.append(pred_crf)

                iou_crf_si = get_iou(pred_crf, gt_n[i])
                iou_crf.append(iou_crf_si)

                ## save_input0
                _pflow = inputs[:, :, 0, :, :][i].data.cpu().numpy().transpose(1, 2, 0)
                cv2.imwrite(join(flow_folder, 't0/%06d.png' % (batch_idx * args.inference_batch_size + i)),
                            rgb2bgr(_pflow))

                ## save_input1
                _pflow = inputs[:, :, 1, :, :][i].data.cpu().numpy().transpose(1, 2, 0)
                cv2.imwrite(join(flow_folder, 't1/%06d.png' % (batch_idx * args.inference_batch_size + i)),
                            rgb2bgr(_pflow))

                ## save_target
                _pflow = target1[i]
                cv2.imwrite(join(flow_folder, 'target/%06d.png' % (batch_idx * args.inference_batch_size + i)),
                            _pflow * 255)

            f1_crf_tem = F1(np.array(pred_crf_), gt_n)
            f1_crf.append(f1_crf_tem)

        progress.update(1)

        if batch_idx == (args.inference_n_batches - 1):
            break

    progress.close()

    f1_mean = 1.0 * np.sum(np.array(f1)) / len(f1)
    iou_mean = 1.0 * np.sum(np.array(iou)) / len(iou)

    f1_crf_mean = 1.0 * np.sum(np.array(f1_crf)) / len(f1_crf)
    iou_crf_mean = 1.0 * np.sum(np.array(iou_crf)) / len(iou_crf)
    return