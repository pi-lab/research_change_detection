import torch
import torch.utils.data as data

import os, math, random
from os.path import *
import numpy as np

from glob import glob
import utils.frame_utils as frame_utils

class StaticRandomCrop(object):
    def __init__(self, image_size, crop_size):
        self.th, self.tw = crop_size
        h, w = image_size
        self.h1 = random.randint(0, h - self.th)
        self.w1 = random.randint(0, w - self.tw)

    def __call__(self, img):
        return img[self.h1:(self.h1+self.th), self.w1:(self.w1+self.tw),:]

class StaticCenterCrop(object):
    def __init__(self, image_size, crop_size):
        self.th, self.tw = crop_size
        self.h, self.w = image_size
    def __call__(self, img):
        return img[(self.h-self.th)/2:(self.h+self.th)/2, (self.w-self.tw)/2:(self.w+self.tw)/2,:]

###############################################################################
# Datareader for PCD-2015
###############################################################################
class Patch(data.Dataset):
    def __init__(self, args, is_cropped = False, root = '', replicates = 1, is_training = True):

        self.args = args
        self.render_size = args.inference_size

        self.is_cropped = is_cropped
        self.replicates = replicates

        if is_training:

            file_list = sorted(glob(join(root, 't0_da', '*.png')))
            name_list = []
            for item in file_list:
                name_list.append(item.split('/')[-1].split('.')[0])

            self.image_list = []
            self.gt_list = []
            for file in name_list:
                img1 = join(root, 't0_da', file+'.png')
                img2 = join(root, 't1_da', file+'.png')
                gt = join(root, 'ground_truth_da', file+'.bmp')

                self.image_list += [[img1, img2]]
                self.gt_list += [gt]
            self.size = len(self.image_list)


        else:
            file_list = sorted(glob(join(root, 't0', '*.jpg')))
            name_list = []
            #for item in file_list[int(len(file_list) * 0.9):]:
            for item in file_list:
                name_list.append(item.split('/')[-1].split('.')[0])

            self.image_list = []
            self.gt_list = []
            for file in name_list:
                img1 = join(root, 't0', file+'.jpg')
                img2 = join(root, 't1', file+'.jpg')
                gt = join(root, 'ground_truth', file+'.bmp')

                self.image_list += [[img1, img2]]
                self.gt_list += [gt]
            self.size = len(self.image_list)
            print self.size



        self.frame_size = frame_utils.read_gen(self.image_list[0][0]).shape

        if (self.render_size[0] < 0) or (self.render_size[1] < 0) or (self.frame_size[0] % 64) or (
            self.frame_size[1] % 64):
            self.render_size[0] = ((self.frame_size[0]) / 64) * 64
            self.render_size[1] = ((self.frame_size[1]) / 64) * 64

        args.inference_size = self.render_size

    def __getitem__(self, index):

        index = index % self.size

        img1 = frame_utils.read_gen(self.image_list[index][0])
        img2 = frame_utils.read_gen(self.image_list[index][1])

        gt = frame_utils.read_gen(self.gt_list[index])

        images = [img1, img2]
        image_size = img1.shape[:2]

        if self.is_cropped:
            cropper = StaticRandomCrop(image_size, self.crop_size)
        else:
            cropper = StaticCenterCrop(image_size, self.render_size)
        images = map(cropper, images)
        gt = cropper(gt)

        images = np.array(images).transpose(3, 0, 1, 2)
        gt = np.array(gt).transpose(2, 0, 1)

        images = torch.from_numpy(images.astype(np.float32))
        gt = torch.from_numpy(gt.astype(np.float32))

        return [images], [gt]


    def __len__(self):
        return self.size * self.replicates


###############################################################################
# Datareader for AICD
###############################################################################
class AICD(data.Dataset):
    def __init__(self, args, is_cropped=False, root='', replicates=1, is_training = True):

        self.args = args
        self.render_size = args.inference_size

        self.is_cropped = is_cropped
        self.replicates = replicates

        self.image_list = []
        self.gt_list = []

        if is_training:
            for scence in range(80):
                for view in range(4):
                    img1 = join(root, 'NoShadow_t0', 'Scene%04d_View%02d_moving.png' % (scence, view))
                    img2 = join(root, 'NoShadow_t1', 'Scene%04d_View%02d_target.png' % (scence, view + 1))
                    gt = join(root, 'GroundTruth', 'Scene%04d_View%02d_gtmask.png' % (scence, view))

                    self.image_list += [[img1, img2]]
                    self.gt_list += [gt]
        else:
            for scence in range(81, 100):
                for view in range(4):
                    img1 = join(root, 'NoShadow_t0', 'Scene%04d_View%02d_moving.png' % (scence, view))
                    img2 = join(root, 'NoShadow_t1', 'Scene%04d_View%02d_target.png' % (scence, view + 1))
                    gt = join(root, 'GroundTruth', 'Scene%04d_View%02d_gtmask.png' % (scence, view))

                    self.image_list += [[img1, img2]]
                    self.gt_list += [gt]


        self.size = len(self.image_list)
        self.frame_size = frame_utils.read_gen(self.image_list[0][0]).shape

        if (self.render_size[0] < 0) or (self.render_size[1] < 0) or (self.frame_size[0] % 64) or (
                    self.frame_size[1] % 64):
            self.render_size[0] = ((self.frame_size[0]) / 64) * 64
            self.render_size[1] = ((self.frame_size[1]) / 64) * 64

        args.inference_size = self.render_size

    def __getitem__(self, index):
        index = index % self.size

        img1 = frame_utils.read_gen(self.image_list[index][0])
        img2 = frame_utils.read_gen(self.image_list[index][1])

        gt = frame_utils.read_gen(self.gt_list[index], True)

        # print np.where(gt == 255)

        images = [img1, img2]
        image_size = img1.shape[:2]

        if self.is_cropped:
            cropper = StaticRandomCrop(image_size, self.crop_size)
        else:
            cropper = StaticCenterCrop(image_size, self.render_size)
        images = map(cropper, images)
        gt = cropper(gt)

        images = np.array(images).transpose(3, 0, 1, 2)
        gt = np.array(gt).transpose(2, 0, 1)

        images = torch.from_numpy(images.astype(np.float32))
        gt = torch.from_numpy(gt.astype(np.float32))

        return [images], [gt]

    def __len__(self):
        return self.size * self.replicates
