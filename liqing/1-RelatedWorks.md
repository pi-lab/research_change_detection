# 相关研究方法



## 1. 研究背景与意义

近年来，随着自主智能体的迅速发展，携带多种传感器的自主智能体广泛应用在农学，地质，森林，海洋，地理测绘，军事侦察，环保等领域。如今，我们在空间和时间这两个维度上，前所未有地精准感知这个世界，快速精确地搜集着各种类型的数据。视觉信息，特别是图像，作为自主智能体获取的较为重要的一种信息，其在环境感知和场景识别中充当了重要的角色。 场景目标检测和变化场景感知是环境感知的两个重要分支。场景目标检测负责空间维度的认知，主要用于识别和定位当前环境中的兴趣物体。而变化场景感知主要用于时间维度的感知，主要检测随着时间流逝当前环境所发生的变化。

场景目标检测是计算机视觉领域的基础研究方向之一，其在视觉场景理解、机器人感知、交通监控等方面具有重要的意义。尤其随着无人驾驶技术成为近几年新的热点，作为无人驾驶技术核心算法之一的目标检测也受到更多的关注。目标检测亦可分成两个子问题：目标定位和目标分类。目标分类问题主要解决从输入图像中判断是否存在有兴趣类别的物体，其输出带有对应置信度的标签以表明兴趣物体出现在图像中某个区域的可能性。目标定位问题负责解决输入图像或某个区域中兴趣物体的位置和大小，其输出物体的外接矩、或物体中心、或物体的轮廓边界等。

## 2. 差异检测算法研究现状

由于差异检测算法可以广泛用于不同领域，现在越来越多的研究者开始关注这个领域的研究，目前也有许多的研究成果。

### 2.1 传统机器学习方法

#### 2.1.1 基于传统机器学习算法训练一个二分类分类器，用来判断像素或区域是否发生变化

Zhang 等人(Object-Based Change Detection for VHR Images Based on Multiscale Uncertainty Analysis, 2018)提出了一种新的基于对象的差异检测技术，该技术首先通过图像分割在相邻尺度之间构造尺度约束，然后利用支持向量机（Support Vector Machnie, SVM）分类进行多尺度不确定性分析。因此，该框架能够对不同尺度的场景目标进行描述和分析。 

Woo 等人(Post-classification change detection of high resolution satellite images using AdaBoost classifier, 2015) 运用自适应 boosting 分类器以分割多时相数据。根据二维共现特征和三维共现特征，使用 adaboost 分类器进行分类，最终生成变化图。

#### 2.1.2 条件随机场（Conditional Random Field, CRF）的检测变化

Zhou 等人（Change Detection Based on Conditional Random Field With Region Connection Constraints in High-Resolution Remote Sensing Images, 2017） 介绍了一种基于条件随机场的差异检测技术，该技术利用模糊c 均值的隶属度作为一元位势，利用相邻像素的欧氏距离来描述成对位势。因此该框架能够实现正确的变化图，避免训练大量的模型参数。

Lv 等人（Unsupervised Change Detection Based on Hybrid Conditional Random Field Model for High Spatial Resolution Remote Sensing Imagery, 2018）将传统的随机场方法与基于对象的技术相结合，提出了一种新的混合条件随机场模型。同一变化区域内的结果是均匀的，并且可以保留变化对象的详细边界。

### 2.2 基于深度学习的方法

近几年，随着深度学习的发展，越来越多的学者开始进行基于深度学习的差异检测研究。由于深度神经网络具有很强的特征提取能力，基于深度学习的方法在总体上获得了较好的性能。

Stent等人(Detecting Change for Multi-View, Long-Term Surface Inspection, 2015)描述了一种基于双通道卷积神经网络的新系统，用于检测隧道表面多个视角的变化。它们利用综合生成的训练示例和隧道表面的均匀性，在训练网络时消除了大部分手动标记工作。(**可以写的更详细，把方法的流程，主要的特点等更详细的罗列出来。在撰写论文的时候仅仅只是摘取简要的描述，但是这里详细的描述方便自己做研究**)



Chianucci等人(Unsupervised change detection using Spatial Transformer Networks, 2016)提出了一种利用空间转换网络（Spatial Transformer Networks, STN）学习执行坐标变换以定位潜在变化区域的深度学习方法。



Alcantarilla等人(Street-View Change Detection with Deconvolutional Networks, 2016)针对汽车自主导航中需要频繁更新大比例尺地图的问题，提出了一种在街景视频中进行结构变化检测的系统。将即时定位映射和三维重建生成的街景数据输入到深度神经网络中进行像素级变化检测。



Khan等人(Learning deep structured network for weakly supervised change detection, 2017)提出了第一个解决弱监督差异检测问题的工作。他们提出了一个全新的CNN模型来检测和定位图像对中的变化，并且修改了mean-field算法以提高定位变化的效率。此外，他们还引入了一个新的卫星图像数据集（GASI-2015）用于训练和验证差异检测算法。



Sakurada等人(Dense optical flow based change detection network robust to difference of camera viewpoints, 2017)提出了一种基于密集光流的差异检测网络。该网络可以从一对拍摄视角不同的图像中检测出场景的变化。通过引入光流信息，这种方法对于摄像机视角之间的变化问题更为稳健。